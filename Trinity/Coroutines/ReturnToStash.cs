﻿using System.Threading.Tasks;
using System.Windows.Data;
using Buddy.Coroutines;
using Trinity.Coroutines.Resources;
using Trinity.DbProvider;
using Trinity.Helpers;
using Trinity.Technicals;
using Zeta.Bot.Coroutines;
using Zeta.Bot.Navigation;
using Zeta.Game;
using Zeta.Game.Internals;
using Zeta.Common;
using Logger = Trinity.Technicals.Logger;

namespace Trinity.Coroutines
{
    public class ReturnToStash
    {
        private static bool _startedOutOfTown;

        public static bool StartedOutOfTown
        {
            get { return _startedOutOfTown; }
            set { _startedOutOfTown = value; }
        }

        public static Vector3 StashLocation
        {
            get
            {
                switch (ZetaDia.CurrentLevelAreaId)
                {
                    case 19947: // Campaign A1 Hub
                        return new Vector3(2968.16f, 2789.63f, 23.94531f);
                    case 332339: // OpenWorld A1 Hub
                        return new Vector3(388.16f, 509.63f, 23.94531f);
                    case 168314: // A2 Hub
                        return new Vector3(323.0558f, 222.7048f, 0f);
                    case 92945: // A3/A4 Hub
                        return new Vector3(387.6834f, 382.0295f, 0f);
                    case 270011: // A5 Hub
                        return new Vector3(502.8296f, 739.7472f, 2.598635f);
                    default:
                        throw new ValueUnavailableException("Unable to get LevelAreaId");
                }
            }
        }

        public static async Task<bool> Execute()
        {
            Logger.LogDebug("Running ReturnToStash");

            if (ZetaDia.Me.IsInCombat)
            {
                Logger.LogDebug("Cannot return to stash while in combat");
                return false;
            }
            if (!ZetaDia.IsInTown && ZetaDia.Me.IsFullyValid() && !ZetaDia.Me.IsInCombat && UIElements.BackgroundScreenPCButtonRecall.IsEnabled)
            {
                StartedOutOfTown = true;
                await CommonCoroutines.UseTownPortal("Returning to stash");
                return true;
            }

            if (!GameUI.IsElementVisible(GameUI.StashDialogMainPage) && ZetaDia.IsInTown)
            {
                // Move to Stash
                if (TownRun.StashLocation.Distance2D(ZetaDia.Me.Position) > 10f)
                {
                    await Navigator.MoveTo(TownRun.StashLocation, "Shared Stash");
                    Trinity.NavServerReport();
                    await Coroutine.Yield();
                }

                if (TownRun.StashLocation.Distance2D(ZetaDia.Me.Position) <= 10f && TownRun.SharedStash == null)
                {
                    Logger.LogError("Shared Stash actor is null!");
                    return false;
                }

                // Open Stash
                if (TownRun.StashLocation.Distance2D(ZetaDia.Me.Position) <= 10f && TownRun.SharedStash != null && !GameUI.IsElementVisible(GameUI.StashDialogMainPage))
                {
                    while (ZetaDia.Me.Movement.IsMoving)
                    {
                        Navigator.PlayerMover.MoveStop();
                        await Coroutine.Yield();
                    }
                    Logger.Log("Opening Stash");
                    TownRun.SharedStash.Interact();
                    await Coroutine.Sleep(200);
                    await Coroutine.Yield();
                    if (GameUI.IsElementVisible(GameUI.StashDialogMainPage))
                        return true;
                    return true;
                }
            }

            Logger.LogVerbose("End ReturnToStash");
            return true;
        }

    }
}
