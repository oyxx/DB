﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using Trinity.Coroutines.Resources;
using Trinity.Helpers;
using Trinity.Technicals;
using Zeta.Game;
using Zeta.Game.Internals.Actors;

namespace Trinity.Coroutines
{
    /// <summary>
    /// Converts crafting materials into other types of crafting materials
    /// </summary>
    public class ConvertMaterials
    {
        public static List<ACDItem> GetBackpackItemsOfQuality(List<ItemQuality> qualities)
        {
            // DB is reporting items as being still there after transmution, add a bunch of checks :(
            return ZetaDia.Me.Inventory.Backpack.Where(i =>
            {
                if (!i.IsValid || i.IsDisposed)
                {
                    //Logger.LogVerbose(LogCategory.Behavior, "[ConvertMaterials] Invalid item '{0}' IsValid/Disposed", i.InternalName);
                    return false;
                }

                if (i.ItemBaseType != ItemBaseType.Armor && i.ItemBaseType != ItemBaseType.Weapon && i.ItemBaseType != ItemBaseType.Jewelry)
                {
                    //Logger.LogVerbose(LogCategory.Behavior, "[ConvertMaterials] Invalid item '{0}' BaseType={1}", i.InternalName, i.ItemBaseType);
                    return false;
                }

                var stackQuantity = i.ItemStackQuantity;
                var isVendor = i.IsVendorBought;
                if (!isVendor && stackQuantity != 0 || isVendor && stackQuantity > 1)
                {
                    //Logger.LogVerbose(LogCategory.Behavior, "[ConvertMaterials] Invalid item '{0}' stackQuantity={1}", i.InternalName, stackQuantity);
                    return false;
                }

                if (!qualities.Contains(i.GetItemQuality()))
                {
                    //Logger.LogVerbose(LogCategory.Behavior, "[ConvertMaterials] Invalid item '{0}' Quality LinkColor={1} DBQuality={2}", i.InternalName, i.GetItemQuality(), i.ItemQualityLevel);
                    return false;
                }

                return true;

            }).ToList();
        }

        public static bool CanRun(CraftingMaterialType from, CraftingMaterialType to, bool excludeLegendaryUpgradeRares = false, bool checkStash = false)
        {
            if (!ZetaDia.IsInGame || !ZetaDia.IsInTown)
                return false;

            if (!Crafting.MaterialConversionTypes.Contains(to) || !Crafting.MaterialConversionTypes.Contains(from))
            {
                Logger.LogVerbose(LogCategory.Behavior, "[ConvertMaterials] Unable to convert from {0} to {1}", from, to);
                return false;
            }

            if (!GetSacraficialItems(to, excludeLegendaryUpgradeRares).Any())
            {
                Logger.LogVerbose(LogCategory.Behavior, "[ConvertMaterials] You dont enough valid weapon/armor/jewellery in backpack", from, to);
                return false;
            }

            Crafting.Materials.Update();
            Crafting.Materials.LogCounts();
    
            if (checkStash)
            {
                if (Crafting.Materials[CraftingMaterialType.DeathsBreath].TotalStackQuantity >= 1 && Crafting.Materials[from].TotalStackQuantity > 100)
                {
                    Logger.LogVerbose(LogCategory.Behavior, "[ConvertMaterials] Enough materials to convert from {0} ({1}) to {2}",
                        from, Crafting.Materials[from].TotalStackQuantity, to);

                    return true;
                }                                                    
            }

            if (Crafting.Materials[from].BackpackStackQuantity > 100)
            {
                Logger.LogVerbose(LogCategory.Behavior, "[ConvertMaterials] We have enough Backpack materials to convert from {0} ({1}) to {2}",
                    from, Crafting.Materials[from].BackpackStackQuantity, to);

                return true;
            }

            Logger.LogVerbose(LogCategory.Behavior, "[ConvertMaterials] Not Enough Backpack materials to convert from {0} ({1}) to {2}, Deaths={3}",
                from, Crafting.Materials[from].BackpackStackQuantity, to, Crafting.Materials[CraftingMaterialType.DeathsBreath].BackpackStackQuantity);

            return false;
        }

        /// <summary>
        /// Converts crafting materials into other types of crafting materials
        /// </summary>
        /// <param name="from">the type of material you will consume</param>        
		/// <param name="to">the type of material you will get more of</param>        
        public static async Task<bool> Execute(CraftingMaterialType from, CraftingMaterialType to)
        {
            Logger.Log("[ConvertMaterials] Wooo! Lets convert some {0} to {1}", from, to);

            if (!ZetaDia.IsInGame || !ZetaDia.IsInTown)
                return true;

            if (!Crafting.MaterialConversionTypes.Contains(to) || !Crafting.MaterialConversionTypes.Contains(from))
            {
                Logger.Log("[Cube] Unable to convert from {0} to {1}", from, to);
                return true;
            }

            var backpackDeathsBreathAmount = Crafting.Backpack.DeathsBreath.Select(i => i.ItemStackQuantity).Sum();
			var backpackFromMaterialAmount = Crafting.Backpack.OfType(from).Select(i => i.ItemStackQuantity).Sum();
			var backpackToMaterialAmount = Crafting.Backpack.OfType(to).Select(i => i.ItemStackQuantity).Sum();

            Crafting.Materials.Update();
            var sacraficialItems = GetSacraficialItems(to);

            Logger.LogVerbose("[ConvertMaterials] Starting Material Counts DeathsBreath={0} {1}={2} {3}={4} SacraficialItems={5}", 
                backpackDeathsBreathAmount, from, backpackFromMaterialAmount, to, backpackToMaterialAmount, sacraficialItems.Count);

            while (CanRun(from, to))
            {
                Crafting.Materials.Update();
                sacraficialItems = GetSacraficialItems(to);

                var item = sacraficialItems.First();
                var transmuteGroup = new List<ACDItem>
				{
                    Crafting.Backpack.DeathsBreath.First(),
                    item
                };
                sacraficialItems.Remove(item);

                // Make sure we include enough materials by adding multiple stacks if nessesary.
                var materialStacks = Crafting.GetMaterialStacksUpToQuantity(Crafting.Backpack.OfType(from), 50).ToList();
                if (materialStacks.Any(m => !m.IsValid || m.IsDisposed) || !item.IsValid || item.IsDisposed)
                {
                    Logger.LogError("[ConvertMaterials] something is terribly wrong our items are not valid");
                    return true;
                }

                transmuteGroup.AddRange(materialStacks);

                await Transmute.Execute(transmuteGroup);
                await Coroutine.Sleep(1500);
                await Coroutine.Yield();

                var newToAmount = Crafting.Backpack.OfType(to).Select(i => i.ItemStackQuantity).Sum();
				if(newToAmount > backpackToMaterialAmount)
				{
					Logger.Log("[ConvertMaterials] Converted materials '{0}' ---> '{1}'", from, to);
					backpackToMaterialAmount = newToAmount;
					backpackFromMaterialAmount = Crafting.Backpack.OfType(from).Select(i => i.ItemStackQuantity).Sum();
					backpackDeathsBreathAmount = Crafting.Backpack.DeathsBreath.Select(i => i.ItemStackQuantity).Sum();
				}
				else
				{
					Logger.LogError("[ConvertMaterials] Failed to convert materials");
					return true;
				}
                
                await Coroutine.Sleep(100);
                await Coroutine.Yield();
            }

            Logger.LogVerbose("[ConvertMaterials] Finishing Material Counts DeathsBreath={0} {1}={2} {3}={4} SacraficialItems={5}",
                backpackDeathsBreathAmount, from, backpackFromMaterialAmount, to, backpackToMaterialAmount, sacraficialItems.Count);

            return true;
        }        

        public static List<ACDItem> GetSacraficialItems(CraftingMaterialType to, bool excludeLegendaryUpgradeRares = false)
        {
            List<ACDItem> sacraficialItems = new List<ACDItem>();

            switch (to)
            {
                case CraftingMaterialType.ReusableParts:
                    sacraficialItems = GetBackpackItemsOfQuality(new List<ItemQuality>
                    {
                        ItemQuality.Inferior,
                        ItemQuality.Normal,
                        ItemQuality.Superior
                    });
                    break;

                case CraftingMaterialType.ArcaneDust:
                    sacraficialItems = GetBackpackItemsOfQuality(new List<ItemQuality>
                    {
                        ItemQuality.Magic1,
                        ItemQuality.Magic2,
                        ItemQuality.Magic3
                    });
                    break;

                case CraftingMaterialType.VeiledCrystal:
                    sacraficialItems = GetBackpackItemsOfQuality(new List<ItemQuality>
                    {
                        ItemQuality.Rare4,
                        ItemQuality.Rare5,
                        ItemQuality.Rare6
                    });
                    break;
            }

            if (excludeLegendaryUpgradeRares)
            {
                var upgradeRares = CubeRaresToLegendary.GetBackPackRares();
                sacraficialItems.RemoveAll(i => upgradeRares.Contains(i));
            }

            return sacraficialItems;
        }
    }
}
