﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Buddy.Coroutines;
using Trinity.DbProvider;
using Zeta.Bot;
using Zeta.Common;
using Zeta.TreeSharp;
using Zeta.Common;
using Zeta.Bot;
using Zeta.Bot.Navigation;
using Zeta.Game;
using Trinity.Technicals;
using Zeta.Game.Internals.Actors;
using Logger = Trinity.Technicals.Logger;

namespace Trinity.Coroutines
{
    public class GetItemsFromStash
    {
        public static bool hasBeenRun;

        public static async Task<bool> Execute(IEnumerable<int> itemIds, int count)
        {
            Logger.Log("Getting Items from Stash, Distance={0}", ReturnToStash.StashLocation.Distance(ZetaDia.Me.Position));

            ZetaDia.Me.UsePower(SNOPower.Walk, ReturnToStash.StashLocation, Trinity.CurrentWorldDynamicId, -1);

            await Navigator.MoveTo(ReturnToStash.StashLocation);
            Trinity.NavServerReport();

            Logger.Log("Finished!");

            return true;
        }

    }
}
