﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using Trinity.Helpers;
using Trinity.Technicals;
using Zeta.Bot;
using Zeta.Game;
using Zeta.Game.Internals;
using Zeta.Game.Internals.Actors;

namespace Trinity.Coroutines.Resources
{
    /// <summary>
    /// Enum CraftingMaterialType - this is not finalized, i don't have all the items
    /// </summary>
    public enum CraftingMaterialType
    {
        None = 0,
        CommonDebris = 1,
        ReusableParts = 361984,
        ArcaneDust = 361985,
        ExquisiteEssence = 3,
        ShimmeringEssence = 4,
        SubtleEssence = 5,
        WishfulEssence = 6,
        DeathsBreath = 361989,
        DemonicEssence = 8,
        EncrustedHoof = 9,
        FallenTooth = 10,
        IridescentTear = 11,
        LizardEye = 12,
        VeiledCrystal = 361986,
        FieryBrimstone = 189863,
        ForgottenSoul = 361988,
        KeyOfBones = 364694,
        KeyOfEvil = 364697,
        KeyOfGluttony = 364695,
        KeyOfWar = 364696,
        KeyOfDestruction = 255880,
        KeyOfHate = 255881,
        KeyOfTerror = 255882,
		CaldeumNightshade = 364281,
		WestmarchHolyWater = 364975,
		ArreatWarTapestry = 364290,
		CorruptedAngelFlesh = 364305,
		KhanduranRune = 365020,
    }

    public enum ItemLocation
    {
        Unknown = 0,
        Backpack,
        Stash,
        Ground,
        Equipped,
    }

    public class Crafting
    {
        static Crafting()
        {
            Materials = new MaterialStore(CraftingMaterialIds);
            Pulsator.OnPulse += Pulsator_OnPulse;
        }

        private static void Pulsator_OnPulse(object sender, EventArgs e)
        {
            if (ZetaDia.IsInGame && ZetaDia.IsInTown && Materials.TimeSinceUpdate.TotalMilliseconds > 1000)
                Materials.Update();
        }

        public static MaterialStore Materials { get; set; }
        public class MaterialStore
        {
            public MaterialStore(IEnumerable<int> actorIds)
            {
                Types = actorIds.Select(i => (CraftingMaterialType)i).ToList();
            }

            public MaterialStore(IList<CraftingMaterialType> types)
            {
                Types = types;
            }

            public IList<CraftingMaterialType> Types { get; set; }

            public Dictionary<CraftingMaterialType, MaterialRecord> Source = new Dictionary<CraftingMaterialType, MaterialRecord>();

            public void Update(bool updateAllProperties = false)
            {
                Source = GetMaterials(Types);
                LastUpdated = DateTime.UtcNow;
            }

            public DateTime LastUpdated = DateTime.MinValue;

            public TimeSpan TimeSinceUpdate
            {
                get { return DateTime.UtcNow.Subtract(LastUpdated);  }
            }

            public MaterialRecord this[CraftingMaterialType i]
            {
                get { return Source[i]; }
                set { Source[i] = value; }
            }

            public void LogCounts(string msg = "", TrinityLogLevel level = TrinityLogLevel.Info)
            {
                Logger.Log(level, msg + " " + GetCountsString(Source));
            }

            public MaterialRecord HighestCountMaterial(IEnumerable<CraftingMaterialType> types)
            {
                return Source.OrderByDescending(pair => pair.Value.TotalStackQuantity).FirstOrDefault().Value;
            }

            public IEnumerable<KeyValuePair<CraftingMaterialType, MaterialRecord>> OfTypes(IEnumerable<CraftingMaterialType> types)
            {
                return Source.Where(m => types.Contains(m.Key));
            }

            public bool HasStackQuantityOfType(CraftingMaterialType type, ItemLocation location, int quantity)
            {
                return Source[type].StackQuantityByInventorySlot(location) > quantity;
            }

            public bool HasStackQuantityOfTypes(IEnumerable<CraftingMaterialType> types, ItemLocation location, int quantity)
            {
                return Source.Where(pair => types.Contains(pair.Key)).All(pair => HasStackQuantityOfType(pair.Key, location, quantity));
            }
        }


        public static IEnumerable<ACDItem> GetMaterialStacksUpToQuantity(List<ACDItem> materialsStacks, int amount)
        {
            if (materialsStacks.Count == 1)
                return materialsStacks;

            long dbQuantity = 0;
            var overlimit = 0;

            // Position in the cube matters; it looks like it will fail if
            // stacks are added after the required amount of ingredient is met, 
            // as the cube encounters them from top left to bottom right.

            var toBeAdded = materialsStacks.TakeWhile(db =>
            {
                var stackQuantity = db.ItemStackQuantity;
                if (dbQuantity + stackQuantity < amount)
                {
                    dbQuantity += stackQuantity;
                    return true;
                }
                overlimit++;
                return overlimit == 1;
            });

            return toBeAdded.ToList();
        }

        public class MaterialRecord
        {
            public int ActorId { get; set; }
            public CraftingMaterialType Type { get; set; }

            public List<ACDItem> StashItems = new List<ACDItem>();
            public List<ACDItem> BackpackItems = new List<ACDItem>();

            public long Total
            {
                get { return StashItemCount + BackpackItemCount; }
            }

            public long StashItemCount
            {
                get { return StashItems.Count; }
            }

            public long BackpackItemCount
            {
                get { return BackpackItems.Count; }
            }

            private long? _backpackStackQuantity;
            public long BackpackStackQuantity
            {
                get { return _backpackStackQuantity ?? (_backpackStackQuantity = BackpackItems.Where(i => i.IsValid && !i.IsDisposed).Select(i => i.ItemStackQuantity).Sum()).Value; }
            }

            private long? _stashStackQuantity;
            public long StashStackQuantity
            {
                get { return _stashStackQuantity ?? (_stashStackQuantity = StashItems.Where(i => i.IsValid && !i.IsDisposed).Select(i => i.ItemStackQuantity).Sum()).Value; }
            }

            private long? _totalStackQuantity;
            public long TotalStackQuantity
            {
                get { return _totalStackQuantity ?? (_totalStackQuantity = StashStackQuantity + BackpackStackQuantity).Value; }
            }

            public long StackQuantityByInventorySlot(ItemLocation slot)
            {
                switch (slot)
                {
                    case ItemLocation.Backpack: return BackpackStackQuantity;
                    case ItemLocation.Stash: return StashStackQuantity;
                    default: return TotalStackQuantity;
                }
            }


        }

        public static string GetCountsString(Dictionary<CraftingMaterialType, MaterialRecord> materials)
        {
            var backpack = string.Empty;
            var stash = string.Empty;
            var total = string.Empty;

            foreach (var item in materials)
            {
                backpack += string.Format("{0}={1} ", item.Key, item.Value.BackpackStackQuantity);
                stash += string.Format("{0}={1} ", item.Key, item.Value.StashStackQuantity);
                total += string.Format("{0}={1} ", item.Key, item.Value.TotalStackQuantity);
            }
            return string.Format("Backpack: [{0}] \r\nStash: [{1}]\r\n Total: [{2}]\r\n", backpack.Trim(), stash.Trim(), total.Trim());
        }

        public static Dictionary<CraftingMaterialType, MaterialRecord> GetMaterials(IList<CraftingMaterialType> types)
        {
            var materials = types.ToDictionary(t => t, v => new MaterialRecord());
            var materialSNOs = new HashSet<int>(types.Select(m => (int)m));

            foreach (var item in ZetaDia.Me.Inventory.Backpack)
            {
                if (!item.IsValid || item.IsDisposed || (item.IsCraftingReagent && item.ItemStackQuantity == 0))
                {
                    Logger.LogVerbose(LogCategory.Behavior, "Invalid item skipped: {0}", item.InternalName);
                    continue;
                }
                    
                var itemSNO = item.ActorSNO;
                if (materialSNOs.Contains(itemSNO))
                {
                    var type = (CraftingMaterialType)itemSNO;
                    var materialRecord = materials[type];
                    materialRecord.BackpackItems.Add(item);
                    materialRecord.Type = type;
                    materialRecord.ActorId = itemSNO;
                }
            }

            foreach (var item in ZetaDia.Me.Inventory.StashItems)
            {
                if (!item.IsValid || item.IsDisposed || (item.IsCraftingReagent && item.ItemStackQuantity == 0))
                {
                    Logger.LogVerbose(LogCategory.Behavior, "Invalid item skipped: {0}", item.InternalName);
                    continue;
                }

                var itemSNO = item.ActorSNO;
                if (materialSNOs.Contains(itemSNO))
                {
                    var type = (CraftingMaterialType) itemSNO;
                    var materialRecord = materials[type];
                    materialRecord.StashItems.Add(item);
                    materialRecord.Type = type;
                    materialRecord.ActorId = itemSNO;
                }
            }
            return materials;
        }

        public static HashSet<CraftingMaterialType> MaterialConversionTypes = new HashSet<CraftingMaterialType>
        {
            CraftingMaterialType.ArcaneDust,
            CraftingMaterialType.ReusableParts,
            CraftingMaterialType.VeiledCrystal,
        };

        public static HashSet<CraftingMaterialType> CraftingMaterialTypes = new HashSet<CraftingMaterialType>
        {
            CraftingMaterialType.ArcaneDust,
            CraftingMaterialType.ReusableParts,
            CraftingMaterialType.VeiledCrystal,
            CraftingMaterialType.CaldeumNightshade,
            CraftingMaterialType.DeathsBreath,
            CraftingMaterialType.WestmarchHolyWater,
            CraftingMaterialType.ArreatWarTapestry,
            CraftingMaterialType.CorruptedAngelFlesh,
            CraftingMaterialType.KhanduranRune,
            CraftingMaterialType.ForgottenSoul,
        };

        public static HashSet<int> CraftingMaterialIds = new HashSet<int>
        {
            361985, //Type: Item, Name: Arcane Dust
            361984, //Type: Item, Name: Reusable Parts
            361986, //Type: Item, Name: Veiled Crystal
            364281, //Type: Item, Name: Caldeum Nightshade
            361989, //Type: Item, Name: Death's Breath
            364975, //Type: Item, Name: Westmarch Holy Water
            364290, //Type: Item, Name: Arreat War Tapestry
            364305, //Type: Item, Name: Corrupted Angel Flesh
            365020, //Type: Item, Name: Khanduran Rune
            361988, //Type: Item, Name: Forgotten Soul
        };

        public static HashSet<int> MaterialConversionIds = new HashSet<int>
        {
            361985, //Type: Item, Name: Arcane Dust
            361984, //Type: Item, Name: Reusable Parts
            361986, //Type: Item, Name: Veiled Crystal
            361989, //Type: Item, Name: Death's Breath
        };

        public static HashSet<int> RareUpgradeIds = new HashSet<int>
        {
            361985, //Type: Item, Name: Arcane Dust
            361984, //Type: Item, Name: Reusable Parts
            361986, //Type: Item, Name: Veiled Crystal
            361989, //Type: Item, Name: Death's Breath
        };

        public static HashSet<int> SetRollingIds = new HashSet<int>
        {
            361989, //Type: Item, Name: Death's Breath
            361988, //Type: Item, Name: Forgotten Soul
        };

        public static HashSet<int> PowerExtractionIds = new HashSet<int>
        {
            364281, //Type: Item, Name: Caldeum Nightshade
            361989, //Type: Item, Name: Death's Breath
            364975, //Type: Item, Name: Westmarch Holy Water
            364290, //Type: Item, Name: Arreat War Tapestry
            364305, //Type: Item, Name: Corrupted Angel Flesh
            365020, //Type: Item, Name: Khanduran Rune
            361988, //Type: Item, Name: Forgotten Soul
        };

        public static class Backpack
        {
            public static List<ACDItem> OfType(CraftingMaterialType type)
            {
                return ZetaDia.Me.Inventory.Backpack.Where(i => i.IsValid && !i.IsDisposed && i.ItemStackQuantity != 0 && i.ActorSNO == (int)type).ToList();
            }
		
            public static List<ACDItem> ArcaneDust
            {
                get { return ZetaDia.Me.Inventory.Backpack.Where(i => i.ActorSNO == (int)CraftingMaterialType.ArcaneDust).ToList(); }
            }

            public static List<ACDItem> ReusableParts
            {
                get { return ZetaDia.Me.Inventory.Backpack.Where(i => i.ActorSNO == (int)CraftingMaterialType.ReusableParts).ToList(); }
            }

            public static List<ACDItem> VeiledCrystals
            {
                get { return ZetaDia.Me.Inventory.Backpack.Where(i => i.ActorSNO == (int)CraftingMaterialType.VeiledCrystal).ToList(); }
            }

            public static List<ACDItem> DeathsBreath
            {
                get { return ZetaDia.Me.Inventory.Backpack.Where(i => i.ActorSNO == (int)CraftingMaterialType.DeathsBreath).ToList(); }
            }
			
            public static List<ACDItem> ForgottenSoul
            {
                get { return ZetaDia.Me.Inventory.Backpack.Where(i => i.ActorSNO == (int)CraftingMaterialType.ForgottenSoul).ToList(); }
            }			
			
            public static List<ACDItem> CaldeumNightshade
            {
                get { return ZetaDia.Me.Inventory.Backpack.Where(i => i.ActorSNO == (int)CraftingMaterialType.CaldeumNightshade).ToList(); }
            }			

			public static List<ACDItem> WestmarchHolyWater
            {
                get { return ZetaDia.Me.Inventory.Backpack.Where(i => i.ActorSNO == (int)CraftingMaterialType.WestmarchHolyWater).ToList(); }
            }		
			
			public static List<ACDItem> ArreatWarTapestry
            {
                get { return ZetaDia.Me.Inventory.Backpack.Where(i => i.ActorSNO == (int)CraftingMaterialType.ArreatWarTapestry).ToList(); }
            }			

			public static List<ACDItem> CorruptedAngelFlesh
            {
                get { return ZetaDia.Me.Inventory.Backpack.Where(i => i.ActorSNO == (int)CraftingMaterialType.CorruptedAngelFlesh).ToList(); }
            }	
			
			public static List<ACDItem> KhanduranRune
            {
                get { return ZetaDia.Me.Inventory.Backpack.Where(i => i.ActorSNO == (int)CraftingMaterialType.KhanduranRune).ToList(); }
            }				
        }

        public static class Stash
        {
            public static List<ACDItem> OfType(CraftingMaterialType type)
            {
                return ZetaDia.Me.Inventory.Backpack.Where(i => i.ActorSNO == (int)type).ToList();
            }		
		
            public static List<ACDItem> ArcaneDust
            {
                get { return ZetaDia.Me.Inventory.StashItems.Where(i => i.ActorSNO == (int)CraftingMaterialType.ArcaneDust).ToList(); }
            }

            public static List<ACDItem> ReusableParts
            {
                get { return ZetaDia.Me.Inventory.StashItems.Where(i => i.ActorSNO == (int)CraftingMaterialType.ReusableParts).ToList(); }
            }

            public static List<ACDItem> VeiledCrystals
            {
                get { return ZetaDia.Me.Inventory.StashItems.Where(i => i.ActorSNO == (int)CraftingMaterialType.VeiledCrystal).ToList(); }
            }

            public static List<ACDItem> DeathsBreath
            {
                get { return ZetaDia.Me.Inventory.StashItems.Where(i => i.ActorSNO == (int)CraftingMaterialType.DeathsBreath).ToList(); }
            }
			
            public static List<ACDItem> ForgottenSoul
            {
                get { return ZetaDia.Me.Inventory.StashItems.Where(i => i.ActorSNO == (int)CraftingMaterialType.ForgottenSoul).ToList(); }
            }			
			
            public static List<ACDItem> CaldeumNightshade
            {
                get { return ZetaDia.Me.Inventory.StashItems.Where(i => i.ActorSNO == (int)CraftingMaterialType.CaldeumNightshade).ToList(); }
            }			

			public static List<ACDItem> WestmarchHolyWater
            {
                get { return ZetaDia.Me.Inventory.StashItems.Where(i => i.ActorSNO == (int)CraftingMaterialType.WestmarchHolyWater).ToList(); }
            }		
			
			public static List<ACDItem> ArreatWarTapestry
            {
                get { return ZetaDia.Me.Inventory.StashItems.Where(i => i.ActorSNO == (int)CraftingMaterialType.ArreatWarTapestry).ToList(); }
            }			

			public static List<ACDItem> CorruptedAngelFlesh
            {
                get { return ZetaDia.Me.Inventory.StashItems.Where(i => i.ActorSNO == (int)CraftingMaterialType.CorruptedAngelFlesh).ToList(); }
            }	
			
			public static List<ACDItem> KhanduranRune
            {
                get { return ZetaDia.Me.Inventory.StashItems.Where(i => i.ActorSNO == (int)CraftingMaterialType.KhanduranRune).ToList(); }
            }				
        }
    }
}
