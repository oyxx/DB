﻿using System;
using Adventurer.Game.Actors;
using Adventurer.Game.Exploration;
using Adventurer.Game.Quests;
using Adventurer.Util;
using Zeta.Bot;
using Zeta.Bot.Logic;
using Zeta.Game;

namespace Adventurer.Game.Events
{
    public enum ProfileType
    {
        Unknown,
        Rift,
        Bounty,
        Keywarden
    }

    public static class PluginEvents
    {
        public static ProfileType CurrentProfileType { get; internal set; }
        public static long WorldChangeTime { get; private set; }
        private static uint _lastUpdate;

        public static long TimeSinceWorldChange
        {
            get
            {
                if (WorldChangeTime == 0)
                {
                    return int.MaxValue;
                }
                return PluginTime.CurrentMillisecond - WorldChangeTime;
            }
        }

        public static void GameEvents_OnWorldChanged(object sender, EventArgs e)
        {
            if (!Adventurer.IsAdventurerTagRunning())
            {
                Logger.Debug("[BotEvents] Reseting the grids.");
                ScenesStorage.Reset();
            }
            else
            {
                BrainBehavior.DungeonExplorer.SetNodesExploredAutomatically = false;
            }

            WorldChangeTime = PluginTime.CurrentMillisecond;
            Logger.Debug("[BotEvents] World has changed to WorldId: {0} LevelAreaId: {1}", AdvDia.CurrentWorldId, AdvDia.CurrentLevelAreaId);
            EntryPortals.AddEntryPortal();

        }

        public static void OnBotStart(IBot bot)
        {
            Pulsator.OnPulse += Pulsator_OnPulse;
        }

        private static void Pulsator_OnPulse(object sender, EventArgs e)
        {
            if (!Adventurer.IsAdventurerTagRunning()) return;
            PulseUpdates();
        }

        public static void PulseUpdates()
        {
            var curFrame = ZetaDia.Memory.Executor.FrameCount;
            if (curFrame == _lastUpdate) return;
            _lastUpdate = curFrame;
            ScenesStorage.Update();
            ExplorationGrid.PulseSetVisited();
            BountyStatistics.Pulse();
        }

        public static void OnBotStop(IBot bot)
        {
            Pulsator.OnPulse -= Pulsator_OnPulse;
            BountyStatistics.Report();
        }




    }
}
