﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Adventurer.Game.Exploration.Algorithms;
using Adventurer.Game.Exploration.Algorithms.AStar;
using Adventurer.Util;
using Zeta.Common;
using Logger = Adventurer.Util.Logger;

namespace Adventurer.Game.Exploration
{
    public sealed class NavigationGrid : Grid
    {
        private static readonly ConcurrentDictionary<int, Lazy<NavigationGrid>> WorldGrids = new ConcurrentDictionary<int, Lazy<NavigationGrid>>();
        private const int GRID_BOUNDS = 2500;

        public static NavigationGrid GetWorldGrid(int worldDynamicId)
        {
            return WorldGrids.GetOrAdd(worldDynamicId, new Lazy<NavigationGrid>(() => new NavigationGrid())).Value;
        }

        public static NavigationGrid Instance
        {
            get { return GetWorldGrid(AdvDia.CurrentWorldDynamicId); }
        }

        public override float BoxSize
        {
            get { return ExplorationData.NavigationNodeBoxSize; }
        }

        public override int GridBounds
        {
            get { return GRID_BOUNDS; }
        }

        public NavigationGrid()
        {
            Updated += NavigationGrid_Updated;
        }

        void NavigationGrid_Updated(object sender, List<INode> newNodes)
        {
            _pathFinder = new PathFinder(InnerGrid);
        }

        protected override bool MarkNodesNearWall
        {
            get { return true; }
        }

        public static void ResetAll()
        {
            WorldGrids.Clear();
        }

        public override bool CanRayCast(Vector3 from, Vector3 to)
        {
            return GetRayLine(from, to).Select(point => InnerGrid[point.X, point.Y]).All(node => node != null && node.NodeFlags.HasFlag(NodeFlags.AllowProjectile));
        }

        public override bool CanRayWalk(Vector3 from, Vector3 to)
        {
            return GetRayLine(from, to).Select(point => InnerGrid[point.X, point.Y]).All(node => node != null && node.NodeFlags.HasFlag(NodeFlags.AllowWalk));
        }

        private IEnumerable<GridPoint> GetRayLine(Vector3 from, Vector3 to)
        {
            var gridFrom = ToGridPoint(from);
            var gridTo = ToGridPoint(to);
            return Bresenham.GetPointsOnLine(gridFrom, gridTo);
        }

        private IPathFinder _pathFinder = new EmptyPathfinder();

        public IndexedList<Vector3> GeneratePath(Vector3 from, Vector3 to)
        {
            var fromGrid = ToGridPoint(from);
            var toGrid = ToGridPoint(to);

            List<PathFinderNode> path;
            using (new PerformanceLogger("[NavigationGrid] GeneratePath", false, false))
            {
                path = _pathFinder.FindPath(fromGrid, toGrid);
            }
            var result = new IndexedList<Vector3>();
            if (path != null)
            {
                if (path.Count > 2)
                {
                    bool skip = false;
                    for (int i = 0; i < path.Count; i++)
                    {
                        if (!skip && i != path.Count - 1)
                        {
                            var pathFinderNode = path[i];
                            var navNode = InnerGrid[pathFinderNode.X, pathFinderNode.Y] as NavigationNode;
                            if (navNode != null)
                            {
                                result.Add(navNode.NavigableCenter);
                            }
                            skip = true;
                        }
                        else
                        {
                            skip = false;
                        }
                    }
                }
                return result;
                //var explorationNodes = new List<ExplorationNode>();
                //foreach (var pathFinderNode in path)
                //{
                //    var navNode = InnerGrid[pathFinderNode.X, pathFinderNode.Y] as NavigationNode;
                //    if (navNode != null && navNode.ExplorationNode != null && navNode.ExplorationNode.HasEnoughNavigableCells)
                //    {
                //        if (!explorationNodes.Contains(navNode.ExplorationNode))
                //            explorationNodes.Add(navNode.ExplorationNode);
                //    }
                //}
                //return new IndexedList<Vector3>(explorationNodes.Select(p => p.NavigableCenter));
            }
            Logger.Debug("[NavigationGrid] Path generation failed.");
            return new IndexedList<Vector3>();
        }

    }
}
