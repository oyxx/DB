﻿using System.Collections.Generic;

namespace Adventurer.Game.Exploration.Algorithms.AStar
{
    public class EmptyPathfinder: IPathFinder
    {
        public event PathFinderDebugHandler PathFinderDebug;

        public bool Stopped { get; private set; }
        public HeuristicFormula Formula { get; set; }
        public bool Diagonals { get; set; }
        public bool HeavyDiagonals { get; set; }
        public int HeuristicEstimate { get; set; }
        public bool PunishChangeDirection { get; set; }
        public bool ReopenCloseNodes { get; set; }
        public bool TieBreaker { get; set; }
        public int SearchLimit { get; set; }
        public double CompletedTime { get; set; }
        public bool DebugProgress { get; set; }
        public bool DebugFoundPath { get; set; }

        public void FindPathStop()
        {
        }

        public List<PathFinderNode> FindPath(GridPoint start, GridPoint end)
        {
            return new List<PathFinderNode>();
        }
    }
}
