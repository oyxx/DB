﻿using System;
using System.Threading.Tasks;
using Zeta.Bot.Navigation;
using Zeta.Common;
using Zeta.Common.Helpers;
using Logger = Adventurer.Util.Logger;


namespace Adventurer.Game.Exploration.Experiments
{

    public class AdventurerNavigationProvider
    {
        public Vector3 Destination { get; private set; }
        public IndexedList<Vector3> CurrentPath { get; private set; }

        public AdventurerNavigationProvider()
        {
            Reset();
        }

        private void Reset()
        {
            Destination = Vector3.Zero;
            CurrentPath = new IndexedList<Vector3>();
        }
        private WaitTimer _waitTimer = new WaitTimer(TimeSpan.FromSeconds(5));

        public MoveResult MoveTo(Vector3 destination, string destinationName = null)
        {
            ScenesStorage.Update();
            if (destination.DistanceSqr(AdvDia.MyPosition) <= 4)
            {
                Reset();
                return MoveResult.ReachedDestination;
            }
            if (Destination != destination || CurrentPath.Count == 0 || _waitTimer.IsFinished)
            {
                Logger.Debug("[Navigatior] Moving towards {0} (Distance: {1})", destination, destination.Distance(AdvDia.MyPosition));
                Destination = destination;
                CurrentPath = NavigationGrid.Instance.GeneratePath(AdvDia.MyPosition, destination);
                if (CurrentPath.Count == 0)
                {
                    return MoveResult.PathGenerationFailed;
                }
                _waitTimer.Reset();
                return MoveResult.PathGenerated;

            }
            Navigator.PlayerMover.MoveTowards(CurrentPath.Current);
            if (CurrentPath.Current.Distance2DSqr(AdvDia.MyPosition) >= 4)
            {
                return MoveResult.Moved;
            }
            if (!CurrentPath.Next())
            {
                return MoveResult.ReachedDestination;
            }
            return MoveResult.Moved;
        }

        public bool Clear()
        {
            Reset();
            return true;
        }

        public float PathPrecision
        {
            get { return 0; }
        }
    }
}
