﻿namespace Adventurer.Game.Exploration.Experiments
{
    public interface ICloneable<T>
    {
        T Clone();
    }
}