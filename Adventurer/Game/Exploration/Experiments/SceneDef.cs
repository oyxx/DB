﻿using System;
using System.Collections.Generic;
using Zeta.Game.Internals;

namespace Adventurer.Game.Exploration.Experiments
{
    public class SceneDef : ICloneable<SceneDef>
    {
        public int SceneSNO { get; private set; }
        public string Name { get; private set; }
        public List<SceneCellDef> Cells { get; private set; }

        public void Create(Scene scene)
        {
            SceneSNO = scene.Mesh.SceneSNO;
            Name = scene.Name;
        }

        public SceneDef Clone()
        {
            var newSceneDef = new SceneDef();
            newSceneDef.SceneSNO = SceneSNO;
            return new SceneDef();
        }
    }

    public class SceneCellDef: ICloneable<SceneCellDef>
    {
        public SceneCellDef Clone()
        {
            throw new NotImplementedException();
        }
    }
}
