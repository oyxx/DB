﻿using System;
using Adventurer.Game.Events;
using Adventurer.Util;
using Zeta.Game;

namespace Adventurer.Game.Stats
{
    public class ExperienceTracker : PulsingObject
    {
        private long _experience;
        private DateTime _startTime;
        private long _lastSeen;
        private bool _isStarted;

        public void Start()
        {
            _startTime = DateTime.UtcNow;
            _experience = 0;
            _lastSeen = GetLastSeen();
            EnablePulse();
            _isStarted = true;
        }

        public void StopAndReport(string reporterName)
        {
            DisablePulse();
            if (_isStarted)
            {
                UpdateExperience();
                ReportExperience(reporterName);
            }
            _isStarted = false;
        }

        private void ReportExperience(string reporterName)
        {
            Logger.Info("[{0}] Total XP Gained: {1:0,0}", reporterName, _experience);
            Logger.Info("[{0}] XP / Hour: {1:0,0}", reporterName, _experience / (DateTime.UtcNow - _startTime).TotalHours);
        }

        private void UpdateExperience()
        {
            var currentLastSeen = GetLastSeen();
            if (_lastSeen < currentLastSeen)
            {
                _experience += (currentLastSeen - _lastSeen);
            }
            _lastSeen = currentLastSeen;
        }

        private static long GetLastSeen()
        {
            return ZetaDia.Me.Level == 70
                ? ZetaDia.Me.ParagonCurrentExperience
                : ZetaDia.Me.CurrentExperience;
        }

        protected override void OnPulse()
        {
            UpdateExperience();
        }
    }
}
