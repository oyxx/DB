﻿using System;
using GreyMagic;
using Zeta.Game;

namespace Adventurer.Util
{
    public class ZetaCacheHelper : IDisposable
    {
        private readonly ExternalReadCache _externalReadCache;
        //private GreyMagic.FrameLock frameLock;
        public ZetaCacheHelper()
        {
            //frameLock = ZetaDia.Memory.AcquireFrame();
            ZetaDia.Actors.Update();
            _externalReadCache = ZetaDia.Memory.SaveCacheState();
            ZetaDia.Memory.TemporaryCacheState(false);
        }

        ~ZetaCacheHelper()
        {
            Dispose();
        }
        public void Dispose()
        {
            //if (frameLock != null)
            //    frameLock.Dispose();

            if (_externalReadCache != null)
                _externalReadCache.Dispose();
        }
    }
}
