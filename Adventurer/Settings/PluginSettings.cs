﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using Adventurer.Game.Rift;
using Adventurer.Util;
using GreyMagic;
using Zeta.Bot;
using Zeta.Game;
using Zeta.Game.Internals;
using Zeta.Game.Internals.Actors;

namespace Adventurer.Settings
{

    [DataContract]
    public class PluginSettings
    {
        private static ConcurrentDictionary<int, PluginSettings> _settings = new ConcurrentDictionary<int, PluginSettings>();
        private AdventurerGems _gems;

        public static PluginSettings Current { get { return _settings.GetOrAdd(AdvDia.BattleNetHeroId, LoadCurrent()); } }

        [DataMember]
        public int GreaterRiftLevel { get; set; }
        [DataMember]
        public bool GreaterRiftRunNephalem { get; set; }
        [DataMember]
        public int GreaterRiftGemUpgradeChance { get; set; }
        [DataMember]
        public bool GreaterRiftPrioritizeEquipedGems { get; set; }
        [DataMember]
        public bool BountyAct1 { get; set; }
        [DataMember]
        public bool BountyAct2 { get; set; }
        [DataMember]
        public bool BountyAct3 { get; set; }
        [DataMember]
        public bool BountyAct4 { get; set; }
        [DataMember]
        public bool BountyAct5 { get; set; }
        [DataMember]
        public bool BountyZerg { get; set; }
        [DataMember]
        public bool BountyPrioritizeBonusAct { get; set; }

        [DataMember]
        public bool? BountyMode0 { get; set; }
        [DataMember]
        public bool? BountyMode1 { get; set; }
        [DataMember]
        public bool? BountyMode2 { get; set; }
        [DataMember]
        public bool? BountyMode3 { get; set; }

        [DataMember]
        public bool NephalemRiftFullExplore { get; set; }

        [DataMember]
        public bool? KeywardenZergMode { get; set; }

        [DataMember]
        public AdventurerGems Gems
        {
            get
            {
                if (_gems == null)
                {
                    _gems = new AdventurerGems();
                }
                var greaterRiftLevel = RiftData.GetGreaterRiftLevel();
                _gems.UpdateGems(greaterRiftLevel, GreaterRiftPrioritizeEquipedGems);
                return _gems;
            }
            set { _gems = value; }
        }

        public string GreaterRiftLevelRaw
        {
            get
            {
                switch (GreaterRiftLevel)
                {
                    case 0:
                        return "Max";
                    case -1:
                    case -2:
                    case -3:
                    case -4:
                    case -5:
                    case -6:
                    case -7:
                    case -8:
                    case -9:
                    case -10:
                        return "Max - " + (GreaterRiftLevel * -1);
                    default:
                        return GreaterRiftLevel.ToString();
                }
            }
            set
            {
                if (value == "Max")
                {
                    GreaterRiftLevel = 0;
                }
                else
                {
                    int greaterRiftLevel;
                    if (int.TryParse(value.Replace("Max - ", string.Empty), out greaterRiftLevel))
                    {
                        GreaterRiftLevel = greaterRiftLevel;
                    }
                    if (value.Contains("Max")) GreaterRiftLevel = GreaterRiftLevel * -1;
                }
            }
        }

        public List<AdventurerGem> GemUpgradePriority { get { return Gems.Gems; } }

        public PluginSettings() { }
        public PluginSettings(bool initializeDefaults)
        {
            if (!initializeDefaults) return;

            GreaterRiftLevel = 1;
            GreaterRiftRunNephalem = true;
            GreaterRiftGemUpgradeChance = 60;
            GreaterRiftPrioritizeEquipedGems = true;
            BountyAct1 = true;
            BountyAct2 = true;
            BountyAct3 = true;
            BountyAct4 = true;
            BountyAct5 = true;
            BountyZerg = true;
            BountyMode0 = true;
            BountyMode1 = false;
            BountyMode2 = false;
            BountyPrioritizeBonusAct = true;
            NephalemRiftFullExplore = false;
        }

        private int highestUnlockedRiftLevel;

        public List<string> GreaterRiftLevels
        {
            get
            {
                var levels = new List<string>();
                highestUnlockedRiftLevel = 1;
                var result = SafeFrameLock.ExecuteWithinFrameLock(() =>
                {
                    highestUnlockedRiftLevel = ZetaDia.Me.HighestUnlockedRiftLevel;
                    if (highestUnlockedRiftLevel == 0 && ZetaDia.Me.Level == 70)
                    {
                        highestUnlockedRiftLevel = 1;
                    }
                    levels = new List<string>();
                    for (var i = 1; i <= highestUnlockedRiftLevel; i++)
                    {
                        levels.Add(i.ToString());
                    }
                }, true);
                if (!result.Success)
                {
                    Logger.Error("[Settings][GreaterRiftLevels] " + result.Exception.Message);
                }
                var highest = highestUnlockedRiftLevel;
                if (highest > 10) highest = 10;
                for (var i = highest - 1; i >= 0; i--)
                {
                    if (i == 0)
                    {
                        levels.Insert(0, "Max");
                    }
                    else
                    {
                        levels.Insert(0, "Max - " + i);
                    }
                }

                return levels;
            }
        }

        public void UpdateGemList()
        {
            if (_gems != null)
            {
                var greaterRiftLevel = RiftData.GetGreaterRiftLevel();
                _gems.UpdateGems(greaterRiftLevel, GreaterRiftPrioritizeEquipedGems);
            }
        }

        public List<int> GemUpgradeChances
        {
            get { return new List<int> { 100, 90, 80, 70, 60, 30, 15, 8, 4, 2, 1, 0 }; }
        }

        public static PluginSettings LoadCurrent()
        {
            var json = FileUtils.ReadFromTextFile(FileUtils.SettingsPath);
            if (!string.IsNullOrEmpty(json))
            {
                var current = JsonSerializer.Deserialize<PluginSettings>(json);
                if (current != null)
                {
                    if (!current.KeywardenZergMode.HasValue)
                        current.KeywardenZergMode = true;
                    if (!current.BountyMode0.HasValue)
                        current.BountyMode0 = true;
                    if (!current.BountyMode1.HasValue)
                        current.BountyMode1 = false;
                    if (!current.BountyMode2.HasValue)
                        current.BountyMode2 = false;
                    if (!current.BountyMode3.HasValue)
                        current.BountyMode3 = false;
                    return current;
                }
            }
            return new PluginSettings(true);
        }

        public void Save()
        {
            var result = JsonSerializer.Serialize(this);
            FileUtils.WriteToTextFile(FileUtils.SettingsPath, result);
            Logger.Info("Settings saved.");
        }
    }

    [DataContract]
    public class AdventurerGems
    {
        [DataMember]
        public List<AdventurerGem> Gems { get; set; }

        public void UpdateGems(int greaterRiftLevel, bool prioritizeEquipedGems)
        {
            var gemsInInventory = new List<AdventurerGem>();
            var result = SafeFrameLock.ExecuteWithinFrameLock(() =>
            {
                if (!ZetaDia.IsInGame || ZetaDia.Me == null || !ZetaDia.Me.IsValid) return;
                gemsInInventory = ZetaDia.Actors.GetActorsOfType<ACDItem>()
                    .Where(i => i.IsValid && i.ItemType == ItemType.LegendaryGem)
                    .Select(i => new AdventurerGem(i, greaterRiftLevel))
                    .Distinct(new AdventurerGemComparer())
                    .OrderByDescending(i => i.Rank)
                    .ToList();
            }, true);
            if (!result.Success)
            {
                Logger.Error("[AdventurerGems][UpdateGems] " + result.Exception.Message);
                return;
            }

            if (gemsInInventory.Count == 0) return;
            if (Gems == null)
            {
                Gems = gemsInInventory;
            }
            else
            {
                var updatedList = new List<AdventurerGem>();
                foreach (var gem in Gems)
                {
                    var inventoryGem = GetMatchingInventoryGem(gem, gemsInInventory);
                    if (inventoryGem != null)
                    {
                        updatedList.Add(inventoryGem);
                        gemsInInventory.Remove(inventoryGem);
                    }
                }
                updatedList.AddRange(gemsInInventory);

                Gems = updatedList;
            }

            Gems = Gems.OrderBy(i => i.MaxRank ? 1 : 0).ToList();
            if (prioritizeEquipedGems)
            {
                Gems = Gems.OrderByDescending(i => i.IsEquiped ? 1 : 0).ToList();
            }
        }

        private AdventurerGem GetMatchingInventoryGem(AdventurerGem gem, List<AdventurerGem> inventoryGems)
        {
            return inventoryGems.FirstOrDefault(g => g.SNO == gem.SNO && g.Rank >= gem.Rank);
        }

    }

    [DataContract]
    public class AdventurerGem
    {
        public int Guid { get; set; }
        [DataMember]
        public int SNO { get; set; }
        [DataMember]
        public int Rank { get; set; }

        public string DisplayRank
        {
            get
            {
                return MaxRank ? "MAX" : Rank.ToString();
            }
        }

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int UpgradeChance { get; set; }
        [DataMember]
        public bool IsEquiped { get; set; }
        [DataMember]
        public bool MaxRank { get; set; }

        public string DisplayName
        {
            get { return string.Format("{0} (Rank: {1}, Upgrade Chance: {2}%)", Name, Rank, UpgradeChance); }
        }

        public AdventurerGem(ACDItem gem, int griftLevel)
        {
            Guid = gem.ACDGuid;
            SNO = gem.ActorSNO;
            Rank = gem.JewelRank;
            Name = gem.Name;
            MaxRank = (Rank == 50 && (SNO == 428355 || SNO == 405796 || SNO == 405797 || SNO == 405803));
            IsEquiped = !MaxRank && gem.InventorySlot == InventorySlot.Socket;
            UpgradeChance = MaxRank ? 0 : CalculateUpgradeChance(griftLevel);
        }

        public void UpdateUpgradeChance(int griftLevel)
        {
            UpgradeChance = CalculateUpgradeChance(griftLevel);
        }

        private int CalculateUpgradeChance(int griftLevel)
        {
            var result = griftLevel - Rank;
            if (result >= 10) return 100;
            if (result >= 9) return 90;
            if (result >= 8) return 80;
            if (result >= 7) return 70;
            if (result >= 0) return 60;
            if (result >= -1) return 30;
            if (result >= -2) return 15;
            if (result >= -3) return 8;
            if (result >= -4) return 4;
            if (result >= -5) return 2;
            if (result >= -15) return 1;
            return 0;
        }
    }

    public class AdventurerGemComparer : IEqualityComparer<AdventurerGem>
    {
        public bool Equals(AdventurerGem x, AdventurerGem y)
        {
            return x.Guid == y.Guid;
        }

        public int GetHashCode(AdventurerGem obj)
        {
            return obj.Guid;
        }
    }

}
